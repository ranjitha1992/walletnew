class Category < ActiveRecord::Base
  has_many :incomes
  has_many :costs
  belongs_to :user
  validates :name, presence: true
  validates :date, presence: true, allow_blank: false
  
  def incomes_total
    total = 0
    incomes.each do |income|
      total += income.amount
    end
    total
  end
  
  def costs_total
    total = 0
    costs.each do |cost|
      total += cost.amount
    end
    total
  end
end
